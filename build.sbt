name := "pack-id"

version := "1.0-SNAPSHOT"

organization := "com.wieck"

scalaVersion := "2.10.3"

scalacOptions ++= Seq(
  "-language:_",
  "-feature",
  "-unchecked",
  "-deprecation")

libraryDependencies += "org.scalatest" %% "scalatest" % "2.0" % "test"