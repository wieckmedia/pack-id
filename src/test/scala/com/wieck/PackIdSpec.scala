package com.wieck

class PackIdSpec extends support.Spec {

  "PackId" - {

    "underlying value can be retrieved" in {
      val foo = PackId("foo")(PackIdFormatter(identity))

      foo.underlying must equal("foo")
    }

    "can use anonymous Formatters" in {
      class Foo

      val foo: PackId[Foo] = PackId("bar")(PackIdFormatter[Foo]("foo-" + _))

      foo.underlying must equal("foo-bar")
    }

    "next" - {
      "must return the same prefix for many iterations" in {
        val rawFormatter = PackIdFormatter[Any](identity)

        val ids = List.fill(16000) {
          PackId.next(rawFormatter).underlying take 26
        }.foldLeft(Map.empty[String, Int]) { (m, id) =>
          m + (id -> (1 + m.getOrElse(id, 0)))
        }

        ids.keys.size must be <= (3)
        ids.keys.size must be >= (2)

        ids.values.max must be >= (8000)
      }
    }
  }

}