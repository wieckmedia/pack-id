package com.wieck

trait PackIdFormatter[T] extends (String => String) {
  def apply(value: String): String
}

object PackIdFormatter {
  def apply[T](formatter: String => String): PackIdFormatter[T] = {
    new PackIdFormatter[T] {
      def apply(value: String): String = formatter(value)
    }
  }
}

class PackId[T](val underlying: String)

object PackId {

  def apply[T](value: String)(implicit format: PackIdFormatter[T]): PackId[T] = {
    new PackId[T](format(value))
  }

  def next[T](implicit format: PackIdFormatter[T]): PackId[T] = {
    apply(Generator.next)(format)
  }

  private object Generator {
    // NOTE: The implementation of a SequentialId
    // here was lifted from TrophyDB.

    import scala.util.Random

    type StateTuple = Pair[String, Int]
    val generator = new Random()
    var genState: StateTuple = state

    val timestamp = System.currentTimeMillis() * 1000
    val nano = System.nanoTime() / 1000
    val offset = timestamp - nano

    def sequential(state: StateTuple): (String, StateTuple) = {
      val seq: Int = state._2
      val pref: String = state._1
      val seqHex = seq.toHexString
      // add leading zeros to be 6 chars long
      val aligned = (1 to (6 - seqHex.size)).map(s => "0").foldRight(seqHex)((b, a) => b + a)
      val result = pref + aligned
      seq >= 0xfff000 match {
        case true => (result, (newPrefix(), increment()))
        case false => (result, (pref, seq + increment()))
      }
    }

    def next: String = {
      val result = sequential(genState)
      generator.synchronized { genState = result._2 }
      result._1
    }

    private def state = (newPrefix(), increment())

    private def newPrefix() = crypto(13)

    private def increment(): Int = generator.nextInt(0xffe - 1) + 1

    private def crypto(size: Int) = {
      val bytes = new Array[Byte](size)
      generator.nextBytes(bytes)

      //integers must be positive only
      bytes.foldLeft("") { (s, byte) =>
        s + "%02x".format(byte)
      }
    }

  }
}