# Overview

PackId generates successive UUIDs whose first N bytes are consistent (i.e.: not actually random; see _Rationale_ for the _Why_).

# Usage

To generate a `PackId[T]` you'll need a `PackIdFormatter[T]`. There's a convenience method to help you out with that:

```scala
implicit val photoPackIdFormatter = PackIdFormatter[Photo]("photo-" + _)
```

If you just wanted a raw id with no additional formatting you could:

```scala
implicit val photoPackIdFormatter = PackIdFormatter[Photo](identity)
```

And of course if you wanted that to be a global default you could:

```scala
package my.project

object IdFormatting {
  implicit val default = PackIdFormatter[Any](identity)
}

// And now wherever you want to use PackId.next you'd just make sure to:
import my.project.IdFormatting.default
```

Classes that use a `PackId` would look something like the following:

```scala
case class Photo(_id: PackId[Photo], filename: String)
```

Once you have that, you can create a formatted id like so:

```scala
val bob = Photo(PackId.next, "bob.jpg")
```

`PackId.next` will look up the `PackIdFormatter[Photo]` Type Class and generate a properly formatted id.

To get at the underlying value you use the `underlying` method of `PackId`:

```scala
bob._id.underlying
```

So to serialize (spray-json used in this example):

```scala
class PackIdFormat[T] extends JsonFormat[PackId[T]] {

  def read(value: JsValue): PackId[T] = {
    value match {
      case JsString(underlying) => new PackId[T](underlying)
      case _ => throw new DeserializationException("PackId must be a String.")
    }
  }

  def write(id: PackId[T]) = JsString(id.underlying)
}

implicit val photoPackIdFormat = new PackIdFormat[Photo]
```

# Rationale

This is useful when the generated value is stored in a BTree or similar data-structure to prevent premature fan-out. CouchDB uses such structures internally, so this library contributes to maintaining fast document creation performance.

It also flexes the Type System to help avoid simple mistakes. Special formatting requirements of the id are clear. There's no opportunity to pass a raw String as the id of a Photo that doesn't meet the required format. It's also difficult (eg, you would have to instantiate a PackId directly) to pass an id to the wrong class since the id is typed explicitly. e.g.:

```scala
myphoto.copy(_id = myvideo._id)
```

Would fail to compile. While you could easily work around this like so:

```scala
myphoto.copy(_id = new PackId[Photo](myvideo._id.underlying))
```

It would be awkward enough to hopefully cause you to question why you're doing such a thing in the first place.

# Use Case

